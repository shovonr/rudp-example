var dgram = require('dgram');
var crypto = require('crypto');
var program = require('commander');

program
  .version('0.0.0')
  .option('-p, --port <n>', 'The port number to bind to (required)')
  .parse(process.argv);

if (!program.port) {
  program.help();
}

var socket = dgram.createSocket('udp4');

socket.bind(program.port);

var hosts = {};

socket.on('message', function (message, rinfo) {
  var key = rinfo.address + ':' + rinfo.port;
  var msg = message.toString('utf8').split(' ');
  if (msg[0] === 'register') {
    hosts[msg[1]] = { address: rinfo.address, port: rinfo.port };
    console.log('Registered %s, from host %s:%s', msg[1], rinfo.address, rinfo.port);
  } else if (msg[0] === 'talk') {
    if (!hosts[msg[1]]) {
      return;
    }
    // Send to client that is requesting to talk.
    var toSend = new Buffer(hosts[msg[1]].address + ':' + hosts[msg[1]].port);
    socket.send(
      toSend,
      0,
      toSend.length,
      rinfo.port,
      rinfo.address
    );

    // Send to the client that is waiting for a request to talk.
    var toSend = new Buffer(rinfo.address + ':' + rinfo.port);
    socket.send(
      toSend,
      0,
      toSend.length,
      hosts[msg[1]].port,
      hosts[msg[1]].address
    );
  }
});

console.log('Socket listening port %s', 5000);
