var rudp = require('rudp');
var dgram = require('dgram');
var fs = require('fs');
var program = require('commander');
var crypto = require('crypto');
var os = require('os');
var path = require('path');

function parseRinfo(value) {
  var message = value.split(':');
  return {
    address: message[0],
    port: message[1]
  }
}

program
  .version('0.0.0')
  .option('-m, --mediator <address>', 'The host that will mediate the communication (required)', parseRinfo)
  .option('-t, --talk <id>', 'Talk to the specified ID')
  .option('-f, --file <file>', 'Path to a file')

program.on('--help', function () {
  console.log('  Example:');
  console.log('');
  console.log('    $ node app.js -m 192.168.2.66:8000');
  console.log('');
});

program
  .parse(process.argv);

if (!program.mediator) {
  program.help();
}

var remoteAddress = program.mediator.address;

// Create a socket without binding it to an address. This is not necessary.
var socket = dgram.createSocket('udp4');

var peerinfo = null;

// Listen for messages, without creating a new client object.
socket.on('message', function (message, rinfo) {
  // If we don't have a peer listening, then to the initial set-up. Otherwise,
  // ignore.
  if (rinfo.address === remoteAddress && peerinfo === null) {
    var msg = message.toString('utf8').split(':');
    peerinfo = {
      address: msg[0],
      port: parseInt(msg[1], 10)
    }
    console.log('Got request to talk to remote host at %s, %s', peerinfo.address, peerinfo.port);

    // We got our request to chat. Now let's actually chat!
    communicate(peerinfo);
  } else {
    console.log(rinfo);
  }
});

if (!program.talk) {
  var id = crypto.randomBytes(4).toString('hex');
  var buf = new Buffer('register ' + id);
  console.log('Requesting to register %s', id);
  socket.send(buf, 0, buf.length, program.mediator.port, program.mediator.address);
} else {
  var buf = new Buffer('talk ' + program.talk);
  console.log('Requesting to talk to %s', program.talk);
  socket.send(buf, 0, buf.length, program.mediator.port, program.mediator.address);
}

function communicate(peerinfo) {
  var client = new rudp.Client(socket, peerinfo.address, peerinfo.port);

  var filename = path.join(os.tmpdir(), crypto.randomBytes(8).toString('hex'));
  var writeStream = fs.createWriteStream(filename);
  var expected = null;
  var written = 0;
  client.on('data', function (data) {
    if (expected === null) {
      expected = data.readUInt32BE(0);
      console.log('Expecting a %s byte(s) large file', expected);
      if (expected === 0) {
        console.log('No file expected.');
        return;
      }
      var buf = new Buffer(data.length - 8);
      data.copy(buf, 0, 8, data.length);
      written += data.length - 8;
      writeStream.write(buf);
      return;
    }

    writeStream.write(data);
    written += data.length;

    if (written >= expected) {
      console.log('You should now be able to view the file at ' + filename);
    }
  });

  if (program.file) {
    try {
      var fileBuffer = fs.readFileSync(program.file);
      var toSend = new Buffer(8 + fileBuffer.length);
      var offset = 0;
      toSend.writeUInt32BE(fileBuffer.length, offset); offset += 8;
      fileBuffer.copy(toSend, offset, 0, fileBuffer.length);
      console.log('Sending the file ' + program.file);
      client.send(toSend);
    } catch (e) {
      console.log(e);
      process.exit(1);
    }
  } else {
    var toSend = new Buffer(8);
    toSend.writeUInt32BE(0, 0);
    console.log('Just waiting on a file.');
    client.send(toSend);
  }
}